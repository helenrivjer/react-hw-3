import React from "react";
import { Component } from "react";
import Card from "../card/card";
import './app.css'

class App extends Component {
    state = {
        info: [],
        id: '',
        done: false,
        items: []
    }
   
    click = (e) => {
        if (e.target.value === 'додати ➕') {
            document.querySelector('.new-position').classList.remove('none')  
        } 
    }

    change = (e) => {
        const value = e.target.value
        this.setState((state)=>{
            return {
                ...state,
                id: Date.now(),
                info: [value, this.state.done, this.state.id],
                items: JSON.parse(localStorage.getItem('items'))
            }
        })
    }  
    
    clearInput = () => {
        document.querySelector('input').value = ''
        this.setState((state)=>{
            return {
                ...state,
                info: ''
            }
        })
    }

    submit =(e) => {
        e.preventDefault()
        if (document.querySelector('input').value === '') return

        document.querySelector('.new-position').classList.add('none')
        this.setState((state)=>{
            return {
                ...state,
                items: [...this.state.items, this.state.info]
            }  
        })
        
        this.clearInput()
        this.state.items = JSON.parse(localStorage.getItem('items')) } 
    
    componentDidMount() {
        this.setState({
           items: JSON.parse(localStorage.getItem('items'))
        }) 
        
    }
    componentDidUpdate() {
        localStorage.setItem("items", JSON.stringify(this.state.items))
   } 
   
    render () { 

    return (
        <>
        <h3>Додаток</h3>
        <header>
        <div className="display-main">Список покупок</div>
            <ul id="list">
                {Array.isArray(this.state.items) ? this.state.items.map((element, i) => {
                return <li key={Math.random()}><Card id={element[2]} info={element[0]} i={i+1}></Card></li>     
            }) : this.state.items = []}
            </ul>

        </header>
        <main>
            <button className ='add' value='додати ➕' onClick={this.click}>додати ➕</button>
            <form className="new-position none" onSubmit={this.submit}>
                <label></label>
                <input type="text" maxLength="40" placeholder="Доповніть ваш список..." onChange={this.change}></input>
            <button className ='save' value='зберегти📀'>зберегти📀</button></form>
        </main>

        </>
    )
    }
}
export default App

