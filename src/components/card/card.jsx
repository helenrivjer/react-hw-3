import React from "react";
import './card.css'

function Card({info, i, id}){
    let items = JSON.parse(localStorage.getItem('items'))
    let done, checkSpan
    items.map((el)=>{ if(id == el[2] && el[1]===true)
        {done = 'done'; checkSpan = 'done-span'}
    })
    
    const click = (e) => {
    let items = JSON.parse(localStorage.getItem('items'))
    if (e.target.value === '✍️редагувати') {
        const editable = e.target.closest('.card')
        const editableName = editable.querySelector('#name')
        editableName.contentEditable='true'
        
    }else if (e.target.value === '❌видалити') {
        e.target.closest('.card').remove()
        const index = items.findIndex((e)=>e[2] === id);
        items.splice(index, 1)
        localStorage.setItem("items", JSON.stringify(items))
      //  JSON.parse(localStorage.getItem('items'))

    }else if (e.target.value === '✅куплено') {
        const done = e.target.closest('.card')
        const doneName = done.querySelector('#name')
        const doneSpan = done.querySelector('span')
        doneName.classList.toggle('done')
        doneSpan.classList.toggle('done-span')
        const index = items.findIndex((e)=>e[2] === id);
        items[index][1] = !items[index][1]

        localStorage.setItem("items", JSON.stringify(items))
        JSON.parse(localStorage.getItem('items'))
    }
    }
    const blur = (e) => {
        let items= JSON.parse(localStorage.getItem('items'))
        const editable = e.target.closest('.card')
        const editableName = editable.querySelector('#name')
        items.map((el)=>{
                if (el[2] === id && el[0] !== editableName.textContent)
                {el[0] = editableName.textContent}
            })
        localStorage.setItem("items", JSON.stringify(items))
        JSON.parse(localStorage.getItem('items'))
    }

    return(
        <>
        <div className="card" id={id}>
            <span className={checkSpan}>{i}</span>
                <div id='name' className={done} contentEditable='false' onBlur={blur}>{info}</div>
                <div id="btn">
                    <button className="btn-action" value = '✍️редагувати' onClick={click}>✍️</button>
                    <button className="btn-action" value = '❌видалити'onClick={click}>❌</button>
                    <button className="btn-action" value ='✅куплено' onClick={click}>✅</button>
                </div>
            </div>
        </>
    )
}

export default Card